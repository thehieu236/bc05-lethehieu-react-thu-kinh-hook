import { useState } from 'react';
import { dataGlasses } from './data';

function BaiTapThuKinh() {
	const [data, setData] = useState(dataGlasses);

	const renderGlasses = () => {
		return dataGlasses.map((glassesItem, index) => {
			return (
				<img
					onClick={() => {
						changGlasses(glassesItem);
					}}
					className="ml-3 p2 border border-width-1"
					style={{ width: '100px', height: '80px', cursor: 'pointer' }}
					key={index}
					src={glassesItem.virtualImg}
					alt=""
				/>
			);
		});
	};

	const changGlasses = (newGlasses) => {
		setData(newGlasses);
	};

	const keyFrame = `@keyframes animChangeGlasses${Date.now()}{
	            from{
	                width: 0;
	                transform: rotate(45deg);
	                opacity: .7,}
	            to{
	                width: 150px;
	                opacity: .7;
	                transform: rotate(0deg)
	            }
	        }`;

	const styleGlasses = {
		width: '150px',
		top: '75px',
		right: '84px',
		opacity: '0.7',
		animation: `animChangeGlasses${Date.now()} 1s`,
	};

	const infoGlasses = {
		width: '250px',
		left: '285px',
		paddingLeft: '15px',
		backgroundColor: 'rgb(255, 127, 0, .5)',
		textAlign: 'left',
		maxHeight: '195px',
		bottom: '0px',
	};

	console.log(data);
	return (
		<div
			style={{
				backgroundImage: 'url(./img/bg.jpg)',
				backgroundSize: '2000px',
				height: '100vh',
			}}>
			<style>{keyFrame}</style>
			<div
				style={{
					backgroundColor: 'rgba(0,0,0,.8)',
					height: '100vh',
				}}>
				<h3
					style={{ background: 'rgba(0, 0,0,.3' }}
					className="text-center text-white p-5">
					TRY GlASSES APP ONLINE
				</h3>

				<div className="container">
					<div className="row mt-5 text-center">
						<div className="col-6 position-relative">
							<img
								className="position-absolute"
								style={{ width: '250px' }}
								src="./img/model.jpg"
								alt=""
							/>
							<img
								style={styleGlasses}
								className="position-absolute"
								src={data.virtualImg}
								alt=""
							/>
							<div style={infoGlasses} className="position-absolute">
								<p style={{ color: '#AB82FF' }} className="font-weight-bold">
									{data.name}
								</p>
								<span style={{ fontSize: '13px', fontWeight: '300' }}>
									{data.description}
								</span>
							</div>
						</div>
						<div className="col-6">
							<img style={{ width: '250px' }} src="./img/model.jpg" alt="" />
						</div>
					</div>
				</div>
				<div className="bg-light container text-center mt-5 p-3 d-flex justify-content-center">
					<div className="row">{renderGlasses()}</div>
				</div>
			</div>
		</div>
	);
}

export default BaiTapThuKinh;

// export default class BaiTapThuKinh extends Component {
// 	state = {
// 		glassesCurrent: {
// 			id: 2,
// 			price: 20,
// 			name: 'HUDNEE D',
// 			virtualImg: './img/v3.png',
// 			description:
// 				'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis?',
// 		},
// 	};

// 	renderGlasses = () => {
// 		return dataGlasses.map((glassesItem, index) => {
// 			return (
// 				<img
// 					onClick={() => {
// 						this.changGlasses(glassesItem);
// 					}}
// 					className="ml-3 p2 border border-width-1"
// 					style={{ width: '100px', height: '80px', cursor: 'pointer' }}
// 					key={index}
// 					src={glassesItem.virtualImg}
// 					alt=""
// 				/>
// 			);
// 		});
// 	};

// 	changGlasses = (newGlasses) => {
// 		this.setState({
// 			glassesCurrent: newGlasses,
// 		});
// 	};

// 	render() {
// 		const keyFrame = `@keyframes animChangeGlasses${Date.now()}{
//             from{
//                 width: 0;
//                 transform: rotate(45deg);
//                 opacity: .7,}
//             to{
//                 width: 150px;
//                 opacity: .7;
//                 transform: rotate(0deg)
//             }
//         }`;

// 		const styleGlasses = {
// 			width: '150px',
// 			top: '75px',
// 			right: '70px',
// 			opacity: '0.7',
// 			animation: `animChangeGlasses${Date.now()} 1s`,
// 		};

// 		const infoGlasses = {
// 			width: '250px',
// 			top: '240px',
// 			left: '270px',
// 			paddingLeft: '15px',
// 			backgroundColor: 'rgb(255, 127, 0, .5)',
// 			textAlign: 'left',
// 			height: '105',
// 		};

// 		return (
// 			<div
// 				style={{
// 					backgroundImage: 'url(./img/bg.jpg)',
// 					backgroundSize: '2000px',
// 					height: '100vh',
// 				}}>
// 				<style>{keyFrame}</style>
// 				<div
// 					style={{
// 						backgroundColor: 'rgba(0,0,0,.8)',
// 						height: '100vh',
// 					}}>
// 					<h3
// 						style={{ background: 'rgba(0, 0,0,.3' }}
// 						className="text-center text-white p-5">
// 						TRY GlASSES APP ONLINE
// 					</h3>

// 					<div className="container">
// 						<div className="row mt-5 text-center">
// 							<div className="col-6">
// 								<div className="position-relative">
// 									<img
// 										className="position-absolute"
// 										style={{ width: '250px' }}
// 										src="./img/model.jpg"
// 										alt=""
// 									/>

// 									<img
// 										style={styleGlasses}
// 										className="position-absolute"
// 										src={this.state.glassesCurrent.virtualImg}
// 										alt=""
// 									/>

// 									<div style={infoGlasses} className="position-relative">
// 										<p
// 											style={{ color: '#AB82FF' }}
// 											className="font-weight-bold">
// 											{this.state.glassesCurrent.name}
// 										</p>
// 										<span style={{ fontSize: '13px', fontWeight: '300' }}>
// 											{this.state.glassesCurrent.description}
// 										</span>
// 									</div>
// 								</div>
// 							</div>
// 							<div className="col-6">
// 								<img style={{ width: '250px' }} src="./img/model.jpg" alt="" />
// 							</div>
// 						</div>
// 					</div>
// 					<div className="bg-light container text-center mt-5 p-3 d-flex justify-content-center">
// 						<div className="row">{this.renderGlasses()}</div>
// 					</div>
// 				</div>
// 			</div>
// 		);
// 	}
// }
